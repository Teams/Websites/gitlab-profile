# Websites Team

[![Matrix Chat #pages:gnome.org](https://img.shields.io/badge/chat-%23pages%3Agnome.org-blue?logo=matrix&labelColor=grey)](https://matrix.to/#/#pages:gnome.org)

The Websites Team is a loose association of people who maintain websites within the GNOME project.

- [Introduction to the Website Team](https://welcome.gnome.org/team/websites/)

## Websites maintained elsewhere

The following pages are not maintained by the Websites team. Please submit issues and contributions in the given repositories:

- [blogs.gnome.org](https://blogs.gnome.org) – [Infrastructure/openshift-images/blogs](https://gitlab.gnome.org/Infrastructure/openshift-images/blogs)
- [circle.gnome.org](https://circle.gnome.org) – [Teams/Circle/](https://gitlab.gnome.org/Teams/Circle)
- [extensions.gnome.org](https://extensions.gnome.org) – [Infrastructure/extensions-web](https://gitlab.gnome.org/Infrastructure/extensions-web)
- [foundation.gnome.org](https://foundation.gnome.org) – [Infrastructure/foundation-web](https://gitlab.gnome.org/Infrastructure/foundation-web)
- [gitlab.gnome.org](https://gitlab.gnome.org) – [Infrastructure/Infrastructure](https://gitlab.gnome.org/Infrastructure/Infrastructure)
- [l10n.gnome.org](https://l10n.gnome.org) – [Infrastructure/damned-lies](https://gitlab.gnome.org/Infrastructure/damned-lies)
- [mail.gnome.org](https://mail.gnome.org) - [Infrastructure/infrastructure](https://gitlab.gnome.org/Infrastructure/infrastructure)
- [mutter.gnome.org](https://mutter.gnome.org) - [GNOME/mutter](https://gitlab.gnome.org/GNOME/mutter/-/tree/main/doc/website)
- [odrs.gnome.org](https://odrs.gnome.org) – [Infrastructure/odrs-web](https://gitlab.gnome.org/Infrastructure/odrs-web)
- [os.gnome.org](https://os.gnome.org/) - [Teams/Releng/gnome-os-site](https://gitlab.gnome.org/Teams/Releng/gnome-os-site)
- [planet.gnome.org](https://planet.gnome.org) – [Infrastructure/planet-web](https://gitlab.gnome.org/Infrastructure/planet-web/)
- [shop.gnome.org](https://shop.gnome.org) – [Infrastructure/Infrastructure](https://gitlab.gnome.org/Infrastructure/Infrastructure)

Our websites rely on the following resource:

- static.gnome.org - [Infrastructure/static-web](https://gitlab.gnome.org/Infrastructure/static-web)

## General issues

If you can't find where a website is maintained or your issue more abstract, please fill your issue in our [General repository](https://gitlab.gnome.org/Teams/Websites/General).
